import axios, { AxiosResponse } from 'axios';
import { ILoginFormShape, ISignUp, ISignUpWithToken } from '../types/auth';

export const authApi = Object.freeze({
    async signUp(user: ISignUp): Promise<ISignUpWithToken | undefined> {
        const data = await axios.post<ISignUp, AxiosResponse<ISignUpWithToken>>(
            'https://lab.lectrum.io/rtx/api/v2/todos/auth/registration',
            user,
        );

        return data?.data;
    },
    async login(credentials: ILoginFormShape) : Promise<ISignUpWithToken> {
        const { email, password } = credentials;
        const { data } = await axios.get<ISignUpWithToken>(
            'https://lab.lectrum.io/rtx/api/v2/todos/auth/login',
            {
                headers: {
                    Authorization: `Basic ${window.btoa(`${email}:${password}`)}`,
                },
            },
        );

        return data;
    },
    async profile() {
        const { data } = await axios.get<ISignUp>(
            'https://lab.lectrum.io/rtx/api/v2/todos/auth/profile', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            },
        );

        return data;
    },
});
