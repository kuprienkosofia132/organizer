import axios from 'axios';
import { ITags } from '../types/tags';

export const tagsApi = Object.freeze({
    async getTagsArray() :Promise<ITags[]> {
        const { data: tags } = await axios.get <ITags[]>(
            'https://lab.lectrum.io/rtx/api/v2/todos/tags',
        );

        return tags;
    },
});
