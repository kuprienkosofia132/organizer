import axios, { AxiosResponse } from 'axios';
import { ITask, ITaskResObj } from '../types/task';

export const tasksApi = Object.freeze({
    async addTask(task: ITask): Promise<AxiosResponse<ITaskResObj>> {
        const data = await axios.post<ITask, AxiosResponse<ITaskResObj>>(
            'https://lab.lectrum.io/rtx/api/v2/todos/tasks',
            task, {
                headers: {
                    authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            },
        );

        return data;
    },

    async getTasks() {
        const tasks = await axios.get('https://lab.lectrum.io/rtx/api/v2/todos/tasks', {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        });
        // @ts-ignore

        return tasks;
    },

    async updateTask(newTask: ITask, id:string) {
        const response = await axios.put<ITask, AxiosResponse<ITaskResObj>>(`https://lab.lectrum.io/rtx/api/v2/todos/tasks/${id}`, newTask, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        });

        return response;
    },

    async deleteTask(idTask: string) {
        const response = await axios.delete(`https://lab.lectrum.io/rtx/api/v2/todos/tasks/${idTask}`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        });
        const isDeleted = response.status === 204;

        return { isDeleted };
    },

    async completeTask(newTask: ITask, id:string) {
        const response = await axios.put(`https://lab.lectrum.io/rtx/api/v2/todos/tasks/${id}`, newTask, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        });

        return response;
    },
});
