// Core
import { FC } from 'react';
import {
    Navigate, Route, Routes,
} from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';
import { Profile } from './components/pages/Profile';

// Components
import { Login } from './components/pages/Login';
import { SignUp } from './components/pages/SignUp';
import { TaskManager } from './components/pages/TaskManager';
import { useErrorMessage } from './hooks/useError';


export const App: FC = () => {
    useErrorMessage();

    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />
            <Routes>
                <Route path = '/login' element = { <Login /> } />
                <Route path = '/signup' element = { <SignUp /> } />
                <Route path = '/task-manager' element = { <TaskManager /> } />
                <Route path = '/profile' element = { <Profile /> } />
                <Route path = '*' element = { <Navigate to = '/login' replace /> } />
            </Routes>
        </>
    );
};

