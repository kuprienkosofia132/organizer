import { FC } from 'react';
import { FieldError, UseFormRegisterReturn } from 'react-hook-form';

type Props = {
    placeholder: string;
    error?: FieldError;
    register: UseFormRegisterReturn;
    type?: string;
};

export const Input: FC<Props> = (props) => {
    const input = (
        <input
            type = { props.type }
            placeholder = { props.placeholder }
            { ...props.register } />
    );

    return (
        <label className = 'label'>
            <span className = 'errorMessage'>{ props.error?.message }</span>
            { input }
        </label>
    );
};
