import * as yup from 'yup';

export const schemaForLogin = yup.object().shape({
    email: yup
        .string()
        .email('Почта должна быть настоящей')
        .required('Поле email обязаткльно к заполнению'),
    password: yup
        .string()
        .required('Поле password обязатлеьно к заполнению')
        .min(8, 'минимальная длина - 8 символов')
        .max(64, 'максимальная длина - 64 символа'),
});
