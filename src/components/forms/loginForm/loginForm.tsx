import { FC, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Link, useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import { useLogin } from '../../../hooks/useLogin';
import { Input } from '../Input';
import { schemaForLogin } from './confiq';
import { useAppSelector } from '../../../lib/redux/init/store';
import { getToken } from '../../../lib/redux/selectors/auth';

export const LoginForm:FC = () => {
    const { loginUser, getProfileUser } = useLogin();
    const navigate = useNavigate();
    const token = localStorage.getItem('token');
    const storageToken = useAppSelector(getToken);

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schemaForLogin),
    });

    const onSubmit = form.handleSubmit(async (credentials: { email : string,
        password: string }) => {
        await loginUser(credentials);
    });

    useEffect(() => {
        if (storageToken !== null) {
            getProfileUser();
        }
    }, [storageToken]);

    useEffect(() => {
        if (token || storageToken) {
            navigate('/task-manager');
        } else {
            navigate('/login');
        }
    }, [token, storageToken]);

    return (
        <section className = 'sign-form'>
            <form onSubmit = { onSubmit }>
                <fieldset>
                    <legend>Вход</legend>
                    <Input
                        placeholder = 'Электропочта'
                        register = { form.register('email') }
                        error = { form.formState.errors.email } />

                    <Input
                        placeholder = 'Пароль'
                        register = { form.register('password') }
                        error = { form.formState.errors.password }
                        type = 'password' />

                    <input
                        type = 'submit' value = 'Войти'
                        className = 'button-login' />
                </fieldset>
                <p>Если у вас до сих пор нет учётной записи, вы можете <Link to = '/signup'>зарегестрироваться</Link></p>
            </form>
        </section>
    );
};
