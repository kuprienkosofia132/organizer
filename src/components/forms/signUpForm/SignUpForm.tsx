import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { FC } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { Input } from '../Input';
import { useSignUp } from '../../../hooks/useSignUp';
import { ISignUp } from '../../../types/auth';
import { schema } from './config';

export const SignUpForm: FC = () => {
    const { submitUser } = useSignUp();
    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (user:ISignUp) => {
        await submitUser(user);
        form.reset();
    });

    return (
        <section className = 'publish-tip sign-form'>
            <form onSubmit = { onSubmit }>
                <fieldset>
                    <legend>Регистрация</legend>
                    <Input
                        placeholder = 'Имя и фамилия'
                        register = { form.register('name') }
                        error = { form.formState.errors.name } />

                    <Input
                        placeholder = 'Электропочта'
                        error = { form.formState.errors.email }
                        register = { form.register('email') } />

                    <Input
                        placeholder = 'Пароль'
                        register = { form.register('password') }
                        error = { form.formState.errors.password }
                        type = 'password' />

                    <Input
                        placeholder = 'Подтверждение пароля'
                        register = { form.register('confirmPassword') }
                        error = { form.formState.errors.confirmPassword }
                        type = 'password' />
                    <input
                        type = 'submit' value = 'Зарегестрироваться'
                        className = 'button-login' />
                </fieldset>
                <p>Перейти к <Link to = '/login'>логину</Link> </p>
            </form>
        </section>
    );
};

