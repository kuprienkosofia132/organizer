import * as yup from 'yup';

export const schema = yup.object().shape({
    name: yup
        .string()
        .min(2, 'минимальная длина - 2 символа')
        .max(50, 'максимальная длина - 50 символов')
        .required('*'),
    email: yup
        .string()
        .email()
        .required('Поле email обязательно для заполнения'),
    password: yup
        .string()
        .min(8, 'минимальная длина - 8 символов')
        .max(64, 'максимальная длина - 64 символа'),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password')], 'Пароли должны совпадать')
        .required('Поле confirmPassword обязательно для заполнения'),
});
