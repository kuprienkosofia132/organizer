import * as yup from 'yup';

export const schemaTask = yup.object().shape({
    title: yup
        .string()
        .min(3, 'Минимальная длина поля title — 3')
        .max(64, 'Максимальная длина поля title — 64')
        .required('*'),
    description: yup
        .string()
        .min(3, 'Минимальная длина поля description — 3')
        .max(250, 'Максимальная длина поля description — 250'),
});
