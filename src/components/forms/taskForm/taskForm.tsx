import { useForm } from 'react-hook-form';
import React, { FC, useEffect, useState } from 'react';

import DatePicker, { registerLocale } from 'react-datepicker';
import ru from 'date-fns/locale/ru';
import { endOfDay } from 'date-fns';
import { yupResolver } from '@hookform/resolvers/yup';
import { ITask } from '../../../types/task';
import { useTask } from '../../../hooks/useTask';
import { useTags } from '../../../hooks/useTags';
import { useAppDispatch, useAppSelector } from '../../../lib/redux/init/store';
import { getTasks } from '../../../lib/redux/selectors/tasks';
import { taskActions } from '../../../lib/redux/actions/task';
import { useUpdate } from '../../../hooks/useUpdate';
import { schemaTask } from './confiq';

registerLocale('ru', ru);
type Props = {
    setShowForm: Function
};

export const TaskForm: FC<Props> = ({ setShowForm }) => {
    const tags  = useTags();
    const { updateTask } = useUpdate();
    const dispatch = useAppDispatch();
    // @ts-ignore
    const { selectedTask } = useAppSelector<ITask | null>(getTasks);
    const [selectedTag, setSelectedTag] = useState({});
    const [startDate, setStartDate] = useState(new Date());
    const { createTask, deleteTask } = useTask();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schemaTask),
    });

    const dirty = form.formState.isDirty;

    const resetForm = () => {
        form.reset(selectedTask);
        setSelectedTag({});
        dispatch(taskActions.resetSelectedTask());
    };

    useEffect(() => {
        if (tags.length && !selectedTask) {
            setSelectedTag(tags[ 0 ].id);
            form.setValue('tag', tags[ 0 ].id);
        }
    }, [tags]);

    useEffect(() => {
        if (selectedTask !== null) {
            form.reset();
            setSelectedTag(selectedTask.tag.id);
            form.setValue('tag', selectedTask.tag.id);
            form.setValue('deadline', selectedTask.deadline);
            form.setValue('completed', selectedTask?.completed || false);
        } else {
            resetForm();
            form.setValue('deadline', endOfDay(new Date()));
        }
    }, [selectedTask]);

    const settingTags =  tags?.map((tag) => {
        return (
            <span
                className = { `tag ${selectedTag === tag.id ? 'selected' : ''}` }
                key = { tag.id }
                onClick = { () => {
                    form.setValue('tag', tag.id);
                    setSelectedTag(tag.id);
                } }
                style = { { color: tag.color, background: tag.bg } }
                { ...form.register('tag') }>
                { tag.name }
            </span>
        );
    });

    const onSubmit = form.handleSubmit(async (task: ITask) => {
        setShowForm(false);
        if (selectedTask !== null) {
            updateTask(task, selectedTask.id);
            dispatch(taskActions.resetSelectedTask());
        } else {
            await createTask(task);
        }
    });

    return (
        <div>
            <form
                onSubmit = { onSubmit }
                defaultValue = { selectedTask !== null ? selectedTask : null }>
                { selectedTask !== null ? <div className = 'head'>
                    <button
                        className = 'button-complete-task'
                        onClick = { () => {
                            form.setValue('completed', true);
                        }
                        }>Завершить</button>
                    <button
                        type = 'button'
                        className = 'button-remove-task'
                        onClick = { () => {
                            deleteTask(selectedTask.id);
                        }
                        } />
                </div> :  <div className = 'head'>
                </div> }
                <div className = 'content'>
                    <label className = 'label'> Задачи
                        <div>
                            <input
                                defaultValue = { selectedTask !== null ? selectedTask.title : '' }
                                { ...form.register('title') }
                                className = 'title'
                                placeholder = 'Пройти интенсив по React, Redux, MobX, TS'
                                type = 'text'
                                name = 'title' />
                        </div>
                    </label>
                    <div className = 'deadline'>
                        <span className = 'label'>Дедлайн</span>
                        <span className = 'date' >
                            <div className = 'react-datepicker-wrapper'>
                                <div className = 'react-datepicker__input-container'>
                                    <DatePicker
                                        onChange = { (date: Date) => {
                                            form.setValue('deadline', date);
                                            setStartDate(date);
                                        } }
                                        locale = 'ru'
                                        dateFormat = { 'd MMM yyyy' }
                                        minDate = { endOfDay(new Date()) }
                                        selected = { selectedTask !== null
                                            ? new Date(selectedTask.deadline)
                                            : startDate }
                                        showDisabledMonthNavigation />
                                </div>
                            </div>
                        </span>
                    </div>
                    <div className = 'description'>
                        <label className = 'label'>
                            Описание
                        </label>
                        <textarea
                            defaultValue = { selectedTask !== null ? selectedTask.description : '' }
                            { ...form.register('description') }
                            className = 'text'
                            name = 'description'
                            placeholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.' />
                    </div>
                    <div className = 'tags'>
                        { settingTags }
                    </div>
                    <div className = 'errors'>
                        <p className = 'error-message'>

                        </p>
                    </div>
                    <div className = 'form-controls'>
                        <button
                            type = 'button'
                            className = 'button-reset-task'
                            onClick = { () => resetForm() }
                            disabled = { !dirty }>Reset</button>
                        <button
                            type = 'submit'
                            className = 'button-save-task'
                            disabled = { !dirty }>Save</button>
                    </div>
                </div>
            </form>
        </div>
    );
};
