export const formatDate = (date: string | number | Date | null):string | null => {
    if (!date) {
        return null;
    }

    const formattedDate = new Date(date).toLocaleDateString('ru-RU', {
        year:  'numeric',
        month: 'short',
        day:   'numeric',
    });

    return formattedDate;
};
