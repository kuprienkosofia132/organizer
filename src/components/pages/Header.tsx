import { Link, NavLink, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useAppDispatch } from '../../lib/redux/init/store';
import { authActions } from '../../lib/redux/actions/auth';
import { toastOptions } from '../../constant/toastOptions';

export const Header = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const token = localStorage.getItem('token');


    const logOut = () => {
        localStorage.removeItem('token');
        dispatch(authActions.resetToken());
        toast.info('Возвращайся скорее)', toastOptions);
        navigate('/login');
    };

    return (
        <nav>
            { !token && <>
                <NavLink to = '/login' className = 'active' >
                Войти
                </NavLink>
                <Link to = '/task-manager' aria-disabled = { true }>
                К задачам
                </Link>
                <NavLink to = '/profile' aria-disabled = { true }>
                Профиль
                </NavLink>
            </>
            }
            { token && <>
                <NavLink to = '/task-manager' aria-disabled = { false }>
                    К задачам
                </NavLink>
                <NavLink to = '/profile' aria-disabled = { false }>
                    Профиль
                </NavLink>
                <button
                    className = 'button-logout'
                    onClick = { () => logOut() }>Выйти</button>
            </>

            }
        </nav>
    );
};
