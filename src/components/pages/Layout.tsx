import { FC, ReactNode } from 'react';
import { Header } from './Header';

type Props = {
    children : ReactNode
};

export const Layout:FC<Props> = (props) => {
    return (
        <>
            <Header />
            { props.children }
        </>
    );
};
