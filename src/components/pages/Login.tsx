import { LoginForm } from '../forms/loginForm/loginForm';
import { Layout } from './Layout';

export const Login = () => {
    return (
        <>
            <Layout>
                <LoginForm />
            </Layout>
        </>
    );
};
