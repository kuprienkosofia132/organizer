import { SignUpForm } from '../forms/signUpForm/SignUpForm';
import { Layout } from './Layout';

export const SignUp = () => {
    return (
        <>
            <Layout>
                <SignUpForm />
            </Layout>
        </>
    );
};
