import { MainPage } from './mainPage';
import { Layout } from './Layout';

export const TaskManager = () => {
    return (
        <>
            <Layout>
                <MainPage />
            </Layout>
        </>
    );
};
