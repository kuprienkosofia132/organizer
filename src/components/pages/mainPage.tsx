import { useEffect, useState } from 'react';
import { TaskForm } from '../forms/taskForm/taskForm';
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/store';
import { getTasks } from '../../lib/redux/selectors/tasks';
import { ITask } from '../../types/task';
import { taskActions } from '../../lib/redux/actions/task';
import { useTask } from '../../hooks/useTask';
import { formatDate } from '../helper/formatDate';

export const MainPage = () => {
    const dispatch = useAppDispatch();
    const { selectedTask } = useAppSelector(getTasks);
    const { tasks } = useAppSelector<{ tasks: ITask[] }>(getTasks);
    const [showForm, setShowForm] = useState(true);
    const [uniqueKey, setUniqueKey] = useState(Math.random());
    const { getTasksUser } = useTask();

    useEffect(() => {
        getTasksUser();
    }, []);

    const getTask = tasks?.length && tasks?.map((task: ITask) => {
        return (
            <div
                className = { `task ${task?.completed ? 'completed' : ''}` }
                key = { task.id }
                onClick = { () => {
                    dispatch(taskActions.selectedTask(task));
                    setShowForm(true);
                    setUniqueKey(Math.random());
                } }>
                <span className = 'title'>
                    { task.title }
                </span>
                <div className = 'meta'>
                    <span className = 'deadline'>
                        { formatDate(task.deadline) }
                    </span>
                    <span
                        className = 'tag'
                        style = { { color: task.tag.color, background: task.tag.bg } }>
                        { task.tag.name }
                    </span>
                </div>
            </div>
        );
    });

    return (
        <>
            <main>
                <div className = 'controls'>
                    <button
                        className = 'button-create-task'
                        onClick = { () => {
                            setShowForm(true);
                            if (selectedTask !== null) {
                                dispatch(taskActions.resetSelectedTask());
                            }
                        }
                        }>
                        Новая задача
                    </button>
                </div>
                <div className = 'wrap'>
                    { !tasks.length
                        ? <div className = 'list empty'> </div>
                        : <div className = 'list'>
                            <div className = 'tasks'>
                                { getTask }
                            </div>
                        </div>
                    }
                    { showForm
                        ?                            <div className = 'task-card'>
                            <TaskForm setShowForm = { setShowForm } key = { uniqueKey } />
                        </div>
                        : null
                    }
                </div>
            </main>

        </>
    );
};
