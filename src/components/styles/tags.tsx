import styled from 'styled-components';

interface IProps {
    color: string,
    background: string,
}

export const Span = styled.span.attrs((props: IProps) => ({
    color:      props.color,
    background: props.background,
}))`
    .tag {
        display: inline-flex;
        justify-content: center;
        width: 92px;
        font-size: 14px;
        padding-top: 9px;
        padding-bottom: 8px;
        border-radius: 4px;
        color: ${(props) => props.color};
        background-color: ${(props) => props.background}
        }
/*
        &.first {
            color: $color7;
            background-color: $color8;
        }

        &.second {
            color: $color9;
            background-color: $color10;
        }

        &.third {
            color: $color11;
            background-color: $color12;
        }

        &.fourth {
            color: $color13;
            background-color: $color14;
        }

        &.fifth {
            color: $color15;
            background-color: $color16;
        } */
    }
`;
