import { useEffect } from 'react';
import { toast } from 'react-toastify';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';
import { toastOptions } from '../constant/toastOptions';
import { authActions } from '../lib/redux/actions/auth';
import { getErrorMessage } from '../lib/redux/selectors/error';

export const useErrorMessage = () => {
    const errorMessage = useAppSelector(getErrorMessage);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (errorMessage) {
            toast.error(errorMessage, toastOptions);

            dispatch(authActions.resetError());
        }
    }, [errorMessage]);
};
