import { ILoginFormShape } from '../types/auth';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';
import { authActions } from '../lib/redux/actions/auth';
import { getToken } from '../lib/redux/selectors/auth';

export const useLogin = () => {
    const dispatch = useAppDispatch();
    const storageToken = useAppSelector(getToken);

    const loginUser = (credentials : ILoginFormShape) => {
        dispatch(authActions.setTokenLogin(credentials));
    };

    const getProfileUser = () => {
        if (storageToken !== '') {
            dispatch(authActions.getProfile());
        }
    };

    return { loginUser, getProfileUser };
};
