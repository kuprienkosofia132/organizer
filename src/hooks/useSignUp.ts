import { ISignUp } from '../types/auth';
import { useAppDispatch } from '../lib/redux/init/store';
import { authActions } from '../lib/redux/actions/auth';

export const useSignUp = () => {
    const dispatch = useAppDispatch();

    const submitUser = (user: ISignUp) => {
        const { confirmPassword, ...signUpUser } = user;
        dispatch(authActions.setTokenSignUp(signUpUser));
    };

    return { submitUser };
};
