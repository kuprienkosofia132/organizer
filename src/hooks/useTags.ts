import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';
import { getTags } from '../lib/redux/selectors/tags';
import { tagsAction } from '../lib/redux/actions/tags';

export const useTags = () => {
    const dispatch = useAppDispatch();
    const tags = useAppSelector(getTags);

    useEffect(() => {
        dispatch(tagsAction.getTagsAsync());
    }, []);

    return tags;
};
