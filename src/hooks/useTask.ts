import { toast } from 'react-toastify';
import { ITask } from '../types/task';
import { taskActions } from '../lib/redux/actions/task';
import { useAppDispatch } from '../lib/redux/init/store';

export const useTask = () => {
    const dispatch = useAppDispatch();
    const createTask = (task: ITask) => {
        dispatch(taskActions.addTaskInform(task));
        toast.success('Задача добавлена');
    };
    const getTasksUser = () => {
        dispatch(taskActions.getAllTasks());
    };
    const deleteTask = (idTask : string) => {
        dispatch(taskActions.deleteTask(idTask));
        toast.info(`Задача с идентификатором ${idTask} удалена`);
    };

    return { createTask, getTasksUser, deleteTask };
};
