import { toast } from 'react-toastify';
import { ITask } from '../types/task';
import { useAppDispatch } from '../lib/redux/init/store';
import { taskActions } from '../lib/redux/actions/task';


export const useUpdate = () => {
    const dispatch = useAppDispatch();

    const updateTask = (task:ITask, id:string) => {
        dispatch(taskActions.updateThunk(task, id));
        toast.info(`Задача с идентификатором ${id} успешно обновлена`);
    };

    return { updateTask };
};
