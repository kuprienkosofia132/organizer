import { toast } from 'react-toastify';
import { typesError } from '../types/error';
import { types } from '../types/auth';
import { AppThunk } from '../init/store';
import { authApi } from '../../../api/signUp';
import { ILoginFormShape, ISignUp, ISignUpWithToken } from '../../../types/auth';
import { toastOptions } from '../../../constant/toastOptions';

export const authActions = Object.freeze({
    setToken: (token: string) => {
        return {
            payload: token,
            type:    types.SET_TOKEN,
            error:   false,
        };
    },
    resetToken: () => {
        return {
            payload: '',
            type:    types.RESET_TOKEN,
            error:   false,
        };
    },
    setTokenLogin: (credentials: ILoginFormShape): AppThunk => async (dispatch) => {
        await authApi.login(credentials).then((token:ISignUpWithToken) => {
            if (token?.data) {
                localStorage.setItem('token', token?.data);
                dispatch(authActions.setToken(token?.data));
                toast.success('Добро пожаловать', toastOptions);
            }


            return token.data;
        }).catch((Error) => {
            if (Error.response.status === 401) {
                dispatch(authActions.setError('не правильный ввод логина или пароля'));
            } else {
                dispatch(authActions.setError('Ошибка запроса'));
            }
        });
    },
    getAuthProfile: (profile: ISignUp) => {
        return {
            payload: profile,
            type:    types.GET_PROFILE,
            error:   false,
        };
    },
    getProfile: () : AppThunk => async (dispatch) => {
        await authApi.profile().then((res) => {
            dispatch(authActions.getAuthProfile(res));

            return res;
        });
    },
    setTokenSignUp: (user: ISignUp): AppThunk => async (dispatch) => {
        await authApi.signUp(user).then((res) => {
            if (res?.data) {
                localStorage.setItem('token', res?.data);
                dispatch(authActions.setToken(res?.data));
            }

            return res?.data;
        }).catch((Error) => {
            if (Error.response.status === 400) {
                dispatch(authActions.setError('пользователь с такой электропочтой уже зарегистрирован'));
            } else {
                dispatch(authActions.setError('Ошибка запроса'));
            }
        });
    },
    setError: (message: string) => {
        return {
            type:    typesError.SET_ERROR,
            error:   true,
            payload: message,
        };
    },
    resetError: () => {
        return {
            type: typesError.RESET_ERROR,
        };
    },
});
