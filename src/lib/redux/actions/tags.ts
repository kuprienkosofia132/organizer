import { AppThunk } from '../init/store';
import { authActions } from './auth';
import { typesTags } from '../types/tags';
import { ITags } from '../../../types/tags';
import { tagsApi } from '../../../api/tags';

export const tagsAction = Object.freeze({

    fetchTags: (tags:ITags[]) => {
        return {
            type:    typesTags.FETCH_TAGS,
            payload: tags,
        };
    },

    getTagsAsync: (): AppThunk => async (dispatch) => {
        await tagsApi.getTagsArray()
            .then((res) => {
                dispatch(tagsAction.fetchTags(res));

                return res;
            }).catch((Error) => {
                const { message } = Error;
                dispatch(authActions.setError(message));
            });
    },
});
