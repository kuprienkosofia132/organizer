import { AxiosResponse } from 'axios';
import {
    ITask, ITaskResObj, ITaskResponse,
} from '../../../types/task';
import { types } from '../types/task';
import { AppThunk } from '../init/store';
import { tasksApi } from '../../../api/task';
import { authActions } from './auth';

export const taskActions = Object.freeze({
    setTask: (task: ITaskResponse) => {
        return {
            payload: task,
            type:    types.SET_TASK,
            error:   false,
        };
    },
    setTasks: (tasks: ITaskResObj[]) => {
        return {
            payload: tasks,
            type:    types.SET_TASKS,
            error:   false,
        };
    },
    updateTask: (updateTask : ITask) => {
        return {
            type:    types.UPDATE_TASK,
            error:   false,
            payload: updateTask,
        };
    },
    deleteTaskFromStorage: (idTask:string) => {
        return {
            type:    types.DELETE_TASK,
            error:   false,
            payload: idTask,
        };
    },
    selectedTask: (selectedTask: ITask) => {
        return {
            type:    types.SELECTED_TASK,
            error:   false,
            payload: selectedTask,
        };
    },
    resetSelectedTask: () => {
        return {
            type:  types.RESET_SELECTED_TASK,
            error: false,
        };
    },
    addTaskInform: (inform:ITask): AppThunk => async (dispatch) => {
        await tasksApi.addTask(inform).then((res:AxiosResponse<ITaskResObj>) => {
            if (res !== undefined) {
                dispatch(taskActions.setTask(res?.data.data));
            }

            return res?.data;
        }).catch((Error) => {
            const { message } = Error;
            dispatch(authActions.setError(message));
        });
    },
    getAllTasks: (): AppThunk => async (dispatch) => {
        await tasksApi.getTasks().then((res) => {
            // @ts-ignore
            dispatch(taskActions.setTasks(res?.data?.data));

            return res.data;
        });
    },
    updateThunk: (updateTask: ITask, id:string):AppThunk => async (dispatch) => {
        await tasksApi.updateTask(updateTask, id).then((res) => {
            dispatch(taskActions.updateTask(res.data.data));

            return res.data.data;
        }).catch((Error) => {
            const { message } = Error;
            dispatch(authActions.setError(message));
        });
    },
    deleteTask: (idTask:string): AppThunk => async (dispatch) => {
        await tasksApi.deleteTask(idTask).then((res) => {
            dispatch(taskActions.deleteTaskFromStorage(idTask));

            return res;
        }).catch((Error) => {
            dispatch(authActions.setError(Error));
        });
    },
});
