// Core
import { combineReducers } from 'redux';

// Reducers
import { authReducers as auth } from '../reducers/auth';
import { taskReducers as task } from '../reducers/task';
import { tagsReducer as tags } from '../reducers/tags';


export const rootReducer = combineReducers({
    auth,
    task,
    tags,
});
