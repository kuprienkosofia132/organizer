import { AnyAction } from 'redux';
import { types } from '../types';
import { typesError } from '../types/error';

const initialState = {
    token:        '',
    errorMessage: '',
    error:        false,
};

export const authReducers = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case typesError.SET_ERROR: {
            return {
                ...state,
                errorMessage: action.payload,
                error:        true,
            };
        }
        case types.GET_PROFILE: {
            return {
                ...state,
                payload:      action.payload,
                errorMessage: '',
                error:        false,
            };
        }
        case typesError.RESET_ERROR: {
            return {
                ...state,
                errorMessage: '',
                error:        false,
            };
        }
        case types.SET_TOKEN: {
            return {
                ...state,
                error: false,
                token: action.payload,
            };
        }
        case types.RESET_TOKEN: {
            return {
                ...state,
                error: false,
                token: '',
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};
