import { AnyAction } from 'redux';
import { typesTags } from '../types/tags';
import { typesError } from '../types/error';

const initialState = {
    tags:         [],
    error:        false,
    errorMessage: '',
};

export const tagsReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case typesTags.FETCH_TAGS: {
            return {
                ...state,
                tags:  action.payload,
                error: false,
            };
        }
        case typesError.SET_ERROR: {
            return {
                ...state,
                error:        true,
                errorMessage: action.payload,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};
