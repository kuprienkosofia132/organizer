import { AnyAction } from 'redux';
import { typesError } from '../types/error';
import { types } from '../types/task';
import { ITask } from '../../../types/task';

const initialState = {
    tasks:        [],
    selectedTask: null,
    errorMessage: '',
    error:        false,
};

export const taskReducers = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case typesError.SET_ERROR: {
            return {
                ...state,
                errorMessage: action.payload,
                error:        true,
            };
        }
        case typesError.RESET_ERROR: {
            return {
                ...state,
                errorMessage: '',
                error:        false,
            };
        }
        case types.SET_TASK: {
            const oldTasks = state.tasks;

            return {
                ...state,
                error: false,
                tasks: [action.payload, ...oldTasks],
            };
        }
        case types.SET_TASKS: {
            return {
                ...state,
                error: false,
                tasks: action.payload,
            };
        }
        case types.DELETE_TASK: {
            const upTask = state.tasks.filter((task:ITask) => {
                return task.id !== action.payload;
            });

            return {
                ...state,
                error: false,
                tasks: upTask,
            };
        }
        case types.UPDATE_TASK: {
            const upTask = state.tasks.map((task:ITask) => {
                return task.id === action.payload.id ? action.payload : task;
            });

            return {
                ...state,
                error: false,
                tasks: upTask,
            };
        }
        case types.SELECTED_TASK: {
            return {
                ...state,
                error:        false,
                selectedTask: action.payload,
            };
        }
        case types.RESET_SELECTED_TASK: {
            return {
                ...state,
                error:        false,
                selectedTask: null,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};
