import { RootState } from '../init/store';

export const getErrorMessage = (state: RootState):string => {
    return state.auth.errorMessage;
};
