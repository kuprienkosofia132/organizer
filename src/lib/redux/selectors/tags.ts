import { RootState } from '../init/store';
import { ITags } from '../../../types/tags';

export const getTags = (store:RootState): ITags[] => {
    return store?.tags?.tags;
};
