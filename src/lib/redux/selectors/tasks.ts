import { RootState } from '../init/store';

export const getTasks = (store:RootState) => {
    return store?.task;
};
