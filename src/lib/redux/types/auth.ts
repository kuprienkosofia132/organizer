export const types = Object.freeze({
    SET_TOKEN:   'SET_TOKEN',
    RESET_TOKEN: 'RESET_TOKEN',
    GET_PROFILE: 'GET_PROFILE',
});
