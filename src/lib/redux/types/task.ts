export const types = Object.freeze({
    SET_TASK:            'SET_TASK',
    SET_TASKS:           'SET_TASKS',
    DELETE_TASK:         'DELETE_TASK',
    UPDATE_TASK:         'UPDATE_TASK',
    RESET_SELECTED_TASK: 'RESET_SELECTED_TASK',
    SELECTED_TASK:       'SELECTED_TASK',
});
