export interface ISignUp {
    name: string;
    email: string;
    password?: string;
    confirmPassword?: string;
}

export interface ISignUpWithToken {
    status: number;
    data: string | undefined;
    statusCode: number
}

export interface ILoginFormShape {
    email: string;
    password: string;
}
