export interface ISignUpFormShape {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}
