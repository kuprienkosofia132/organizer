export interface ITags {
    id: string,
    name: string,
    color: string,
    bg: string
}
