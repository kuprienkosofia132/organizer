import { ITags } from './tags';

export interface ITask {
    id: string;
    completed?: boolean;
    title: string;
    description?: string;
    deadline: string;
    tag: ITags;
}

export interface ITaskResponse {
    id: string;
    created: true;
    title: string;
    description: string;
    deadline: string;
    tag: ITags
}

export interface ITaskResObj {
    data: ITaskResponse
}
